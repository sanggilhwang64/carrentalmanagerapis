package co.kr.psqcloud.api.share;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiShareApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiShareApplication.class, args);
    }
}
