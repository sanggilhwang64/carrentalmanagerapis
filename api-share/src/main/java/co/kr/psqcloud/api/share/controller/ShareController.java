package co.kr.psqcloud.api.share.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/test")
public class ShareController {
    @GetMapping("/share")
    public String getShare() {
        return "ok";
    }
}
