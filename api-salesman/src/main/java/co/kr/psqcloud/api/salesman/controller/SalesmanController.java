package co.kr.psqcloud.api.salesman.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/test")
public class SalesmanController {
    @GetMapping("/Salesman")
    public String getSales() {
        return "ok";
    }
}
