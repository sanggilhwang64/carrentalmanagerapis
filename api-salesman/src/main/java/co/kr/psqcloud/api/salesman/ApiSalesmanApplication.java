package co.kr.psqcloud.api.salesman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSalesmanApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiSalesmanApplication.class, args);
    }
}
