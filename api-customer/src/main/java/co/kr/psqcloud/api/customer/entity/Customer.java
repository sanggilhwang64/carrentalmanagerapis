package co.kr.psqcloud.api.customer.entity;

import co.kr.psqcloud.api.customer.model.CustomerRequest;
import co.kr.psqcloud.common.enums.CounselingType;
import co.kr.psqcloud.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Customer {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "고객명")
    @Column(nullable = false, length = 20)
    private String customerName;

    @ApiModelProperty(notes = "고객연락처")
    @Column(nullable = false, length = 13)
    private String customerPhone;

    @ApiModelProperty(notes = "상담타입")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private CounselingType counselingType;

    @ApiModelProperty(notes = "상담요청시간")
    @Column(nullable = false)
    private LocalTime requestTime;

    private Customer(CustomerBuilder builder) {
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.counselingType = builder.counselingType;
        this.requestTime = builder.requestTime;
    }

    public static class CustomerBuilder implements CommonModelBuilder<Customer> {
        private final String customerName;
        private final String customerPhone;
        private final CounselingType counselingType;
        private final LocalTime requestTime;

        public CustomerBuilder(CustomerRequest request) {
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.counselingType = request.getCounselingType();
            this.requestTime = LocalTime.now();
        }


        @Override
        public Customer build() {
            return new Customer(this);
        }
    }

}
