package co.kr.psqcloud.api.customer.model;

import co.kr.psqcloud.api.customer.entity.Customer;
import co.kr.psqcloud.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerItem {
    @ApiModelProperty(value = "시퀀스")
    private Long id;
    @ApiModelProperty(value = "고객명")
    private String customerName;
    @ApiModelProperty(value = "고객연락처")
    private String customerPhone;
    @ApiModelProperty(value = "상담타입")
    private String counselingType;
    @ApiModelProperty(value = "상담요청시간")
    private LocalTime requestTime;

    private CustomerItem(CustomerItemBuilder builder) {
        this.id = builder.id;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.counselingType = builder.counselingType;
        this.requestTime = builder.requestTime;
    }

    public static class CustomerItemBuilder implements CommonModelBuilder<CustomerItem> {
        private final Long id;
        private final String customerName;
        private final String customerPhone;
        private final String counselingType;
        private final LocalTime requestTime;

        public CustomerItemBuilder(Customer customer) {
            this.id = customer.getId();
            this.customerName = customer.getCustomerName();
            this.customerPhone = customer.getCustomerPhone();
            this.counselingType = customer.getCounselingType().getCounselingName();
            this.requestTime = customer.getRequestTime();
        }


        @Override
        public CustomerItem build() {
            return new CustomerItem(this);
        }
    }
}
