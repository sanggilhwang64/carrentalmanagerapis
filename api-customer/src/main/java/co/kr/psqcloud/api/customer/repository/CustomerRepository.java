package co.kr.psqcloud.api.customer.repository;

import co.kr.psqcloud.api.customer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
