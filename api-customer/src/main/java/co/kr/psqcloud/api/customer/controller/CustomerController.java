package co.kr.psqcloud.api.customer.controller;

import co.kr.psqcloud.api.customer.model.CustomerItem;
import co.kr.psqcloud.api.customer.model.CustomerRequest;
import co.kr.psqcloud.api.customer.service.CustomerService;
import co.kr.psqcloud.common.response.model.CommonResult;
import co.kr.psqcloud.common.response.model.ListResult;
import co.kr.psqcloud.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "고객관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @ApiOperation(value = "고객등록")
    @PostMapping("/new")
    public CommonResult setCustomer(@RequestBody @Valid CustomerRequest customerRequest) {
        customerService.setCustomer(customerRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "고객목록")
    @GetMapping("/all")
    public ListResult<CustomerItem> getCustomers() {
        return ResponseService.getListResult(customerService.getCustomers(),true);
    }
}
