package co.kr.psqcloud.api.customer.service;

import co.kr.psqcloud.api.customer.entity.Customer;
import co.kr.psqcloud.api.customer.model.CustomerItem;
import co.kr.psqcloud.api.customer.model.CustomerRequest;
import co.kr.psqcloud.api.customer.repository.CustomerRepository;
import co.kr.psqcloud.common.response.model.ListResult;
import co.kr.psqcloud.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer.CustomerBuilder(request).build();

        customerRepository.save(addData);
    }

    public ListResult<CustomerItem> getCustomers() {
        List<Customer> originList = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new CustomerItem.CustomerItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
