package co.kr.psqcloud.api.customer.model;

import co.kr.psqcloud.common.enums.CounselingType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 13, max = 13)
    private String customerPhone;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CounselingType counselingType;
}
