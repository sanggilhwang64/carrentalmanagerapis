package co.kr.psqcloud.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CounselingType {

    RENT("렌트"),
    LEASE("리스"),
    UNDETERMINED("미정")
    ;
    private final String CounselingName;
}
